// ==UserScript==
// @name        cyberForumJsFiddleSync
// @description sync jsfiddle on cyberforum.ru
// @author      Nikita Davidenko(nbytes|ntlinuxnt) and BANO.notIT
// @license     GPL v2
// @version     1.0
// @include     *://*.cyberforum.ru/*
// @include     *://jsfiddle.net/*
// @run-at      document-start
// ==/UserScript==

(function (w, undefined) {
    "use strict";


    var IframeSettings = {
        // порядок табов в iframe соответствует порядку их в этом массиве
        // так же отсутствие элемментов массива означает отсутствие соответствующего таба
        tabs: ["html", "js", "css", "result"],
        // использовать тёмныу тему
        dark: true,
        // являются ли спойлеры с кодом сразу открытыми
        open: true,
        // какие коды вставлять вместе с ссылкой
        codes: ["css", "js", "html"],
        // если этот параметр будет равен false, то не будут показываться кнопки кодов, указаных в codes
        showAllButtons: true
    };

    // две части скрипта, каждая зависит от своего url
    if (/^www\.cyberforum\.ru$/.test(w.location.hostname))
        (function () {
            // если находимся на кибер форуме, то...
            var query = document.querySelectorAll.bind(document),
                addListener = function (elem, type, listener, cat) {
                    elem.addEventListener(type, listener, cat || false);
                };

            var css = '\
            .spoiler > input + label:before {\
                content: "+";\
                float: left;\
                margin-right: 3px;\
                font: bold 14px Tahoma\
            }\
\
            .spoiler > input:checked + label:before {\
                content: "-";\
                float: left;\
                margin-right: 3px;\
                font: bold 14px Tahoma\
            }\
\
            .spoiler > input {\
                display: none\
            }\
\
            .spoiler > input + label {\
                padding: 5px 15px;\
                overflow: hidden;\
                width: 100%;\
                box-sizing: border-box;\
                display: block;\
                cursor: pointer;\
            }\
\
            .spoiler > input + label + .text {\
                display: none;\
            }\
\
            .spoiler > input:checked + label + .text {\
                display: block;\
            }\
\
            .spoiler > .text {\
                border-top: none;\
            }\
            \
            .spoiler label{\
                border: 1px solid rgb(215, 215, 215);\
                font-weight: bold;\
            }\
            \
            .fbtn{\
                background: transparent url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABGdBTUEAALGPC/xhBQAABURJREFUWAntVn1sU1UUP+e91+4DaMc+2GibyNfUEAwMUYKEBIliCApEQINRjDHBBCRZZ/iIgDTRSBTom4GoUfcXfiSKiRERgyzIH4hEJegkKCgzuNaVTba2DNq1715/t9vrWlaQ6f6Tm7T33nN+5+Oee865j+jmuBmB4YjA8g/1ylc6PBRoLR6qOh6qgI2v3n5xisMw/JLpHpY8gZic/bx2kvJzImtPm3/0Vzb+WvOQHaje3j7GcJQ2MfGDUso4ER8gKc6wxudhpFwSTYHShaCPJpJfiET6mfDGcsUrOIbkQM2O+GRdl/tx2koIvtSTSL/ZtbE8Okhz4JTT6/athpNbJXEvs1jUVu8+PggHwg07UBHs9JZozhaSfFmK9MLQc6N/KKQwl+Z7NVornbyfJVWkSNwV8Zedy+WrdUEHPGb3dI20R8Gehju+CAWHJcvFJGlOOi2mRtaVtV6t6Fr7GrNrnE76tzD0e5vfdTdwuKWBMcgBT2P8BRjcAlwSsB/7ws21SkRKWh3yj3pjQPzGVh4zukJj7X0hxWNhv/uDa0p5zfgyX2Nc+hpj7yDZRthAz47u+72NsX3YD3LYxvzDzD4z/pPPjB25Ls5rxlph6CgFAtp1gf+C6THjW72N8bQrGC3PFc8aUuXFzOOQZO/CAZELGpa1tI4hfPpIybfn6jOyG91ZrdYARbK0nIVKTCJtGpOMhE6fPUhvzUjlsDNLlXBI3ln4JZOpK193rB/TbmMsS4Q1TSeNeKxNU3NfBBByBxuPK4IkeZua7eF9OVaBa9mvsf69xtzErH3mm3zr6bHB7hk2BjN7g9EXdTZ+VcmGU3xc5CxpRfKtsTG6w0gh/SUs7vKYsSU2PeOAz+XfSszrJYnXQ9E/dtpMNXMJNeF/rhC0RiRStwghF4Gs4TRHvGb3PIVB5WxhTdtMJN6WFg6QSk1V7RjO7PaZ0QUKE653/UIyPR0HPI+r3usJxhYrOqvQMuoUbbUp3OBapYj2yPR7h6OFhKhva3C/NkBX7XjElzAy0SK5HKeYgK7nCvld22wMBQ4bPveME+gf8VC9a/YAvbXY6644hAPXpVM94xFV7VlmeSkVSzRkQf0Lw9Dr1DJlyU9zeZF1NRd6e688oGg6a/tw6m/yjCtG4N60IPUo8TTskFr9IzA+gWb2BHZFhlFar+Elm48G80lHYMwlG5OdBXeptcaUVzqKlhbGFUvoj6i1bmgfqVxR67whqRyWu0HL6359nVQewFU8pKHVeuDgmTzB/o2V5OO4mh4Y2JTbG3w740tLixydxKJHpuVclG4NlXBzpdmRzfCqYHQSa7QC8s2FdMOlFtAnqTIU8DJTglcD258f1eEJRjfimnb53A3HyPQfINZqcZwVONSx9uhfxwkh9QS7lwCzt5iLTqHbNeEFdKBcn0RkE0lKbrhab9+eS8A3cAX0MxLlzsIgZG+DezdZtAxKnaihTRCaw1JuD/0WmaeMK7lwQ9lBQTwTjh1Fcq1FTq1EZJupN1XX6a/6s5BuOAib8hyhPW7GT4wNxuYUAubR8OmVty+06cMMJF0BjGdnVx3enZSnMbaNae3ZIu/E6pPA6dZlnq3CXkBm2EhVgQsjnWUlR1HC7mRv8g70pdokWfIpJKJXL6UWvFoP06rvHMNm0VaEbouec5/TXYyPGpqM3vJ054aqeDZUma8XB+9BacyETAId6ySYqoT+80DejEInmIpmNRLGTwmyVob9ZSeU4qwDGSu4P++s+UuR6QvAGI+kGvJndiFvoasXptrQmA6Fz0Xey0S9EPAm7X8Zgb8BVowAjPMF30EAAAAASUVORK5CYII=) no-repeat scroll center center;\
                width: 35px;\
                height: 28px;\
                cursor: pointer;\
            }\
            .cbfBtn {\
                border: 2px solid #5baef6;\
                color: black;\
                padding: 4px;\
                text-decoration: none;\
                font-family: sans-serif;\
                background: #EBF0FC;\
                transition: 0.5s;\
                cursor: pointer;\
                width: 12%;\
                margin-bottom:5px;\
                display: inline-block;\
            }\
\
            .cbfBtn:hover {\
                border-color: #373790;\
                color: #295b95;\
            }\
            .fbtn-import {\
                width: 35px;\
                height: 28px;\
                cursor: pointer;\
                background:transparent url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AcWABcDgUiwXQAABUhJREFUWMPtl2tsFFUUx3/nzuyWUrtb2krpzkYFgxo1kZcPNEZj4isafNZXoh81QU3YooaAj40aX9BOjUaNyicDJlYTI6JGJWoUFV9RkaCi1mB3bSnS7i5I2925xw9sy7RdQNSIHzzJTSZ37pzzP+f+7/+eAZDyOCRmQs/yXwAghwpAOLgcCgBaDmz+UiVaXnAaH+5LkO6adLAAJDSiQADY8tD9fdi0fMeJEddNqXC6qMxAiJZf9aD6GgTPdaemvHug+OGsxwOwlQP3THUjk1cKcrGqFkBeR+33YmQrUK9wosBFIFNA37CDpZuyS+q37iP50ewdoAoohUDo+CpMW1E43nF0LUKjwP27BktP9S+pz01wnd4U9eLJhYLco8iwiF3QvSi+oQIADVdg0v4ANLRv96pNdCMqv6stXZRZPOWrA+1v8pHcTI3KWlEaitiTe1N1P40n+ggAB4iUgwfND2dnGXdyixhnlgo7RHlHRS9BObNUsif13l7X9WdJNs3vP8rB+VTg5+5U7JQQ4cdUIFqeKDWv+G2ZiLtMjAwBX+8pt8wEUGVhJlX75MEyPeHnrjViVlu112VT8efDAAgBqGp+6Nervfacev7As03Le2pGHawYONfryK/5GxohSb/wTdLPvzeu8mYEgAtEEm07uhLt/etpaXH+acFJ+IV7vI5CKdaeqw8R3xkVnsalnzWKOEehsorOzuAflzwNPhJwDlM5LjztlgFYU9s0FRGMmJ7KGQzMATNL0N7M5i1v8vS8YiXCGcx8gxkaKu7+sO+OqaO+gsBmjXEwSHNIfY0BLC0tRKK114kIih4bduo9kG/wOvJrjTifG5GVIubV5PHHbG5uH5gX3mOvPXefI+4PRsxqhJeqotVdCT9388gCJ+IWFRTDYwk/f8kICANocv6zd4vj3KHYJzK5X9rGsKealSBnW8vNdrB4pLW6ADDGOO95/sA5AImOwl1izJ1gn9FAj6VYPAnV14yYx5N+7kKA7KLYd2hpjqJbReTF5hU7FgAqCX9gjuB8qqors62xGyfofSSyEWsXdbfGHx0rxzVvoXp0gLYYmCFILJOKPbhXDd9xk/F5oqKlzKLYXkVNd03y4g1vIzK7uHPbEUbE3CKiO4v5wdbxe+q6zmyAYqCvhOd7b5+2bXh49/kAjpg1GujH4eCen4948blFRYdRrOfn3b0Apg+WSvZ6oCpSc/gZRlTOU+XlvvTUnROYZ6UfwAj141+VrLs7sM5VAI5rOr0H8g37OwNjE6jrAn1dRBYbFRIg31f6KhiSDaq6y3HNMtLp0e4p2Va4YnJVZDtid2lJz0ZlGtWyrtHvaw6drn11XiOQNgKnGMAKNFUC0LO0tk9Vl4Bcloy3fpT0c+lkR2GVOnQi+klPfvuG7G2xD6wGl4LOmCRVm5J+fnkFxYxUEMdqVVwjyrcqOndftcu2xh8n4EpFoopZpsqZoro882PvOaSnD+5ZU/emRU5VWI/IrUBNyEUfUJzYDOhc0E3idRTuBO61Vs/6tTX2/oFaLzqvCg60xjv9Ai3frCMWzaRioyASbf2zxbifqOgtwq1bqryjm74EnOB3OaNnaW3f31Vdz89HgaHQVHUmFRsEODy97bBoXfV6VONDw0MnCIDXljsNx6wDCqIs7N783ZpKUvsng+u+j4KNCGYLSBIbXJBZXLdOxnQvEXlORE4FBhX9UmDgIONfDkRUyVXoAKOCCMoXluCGbKrui4n/AC0vON78865AzIUC03VPm3awdhOQKZNv5PKvAcGi12R/6l3FYzOH/pWfjv1tx//2n7E/ABuoJS3qdUBxAAAAAElFTkSuQmCC) no-repeat scroll center center;\
            }';

            var node = document.createElement("style");
            node.type = "text/css";
            node.appendChild(document.createTextNode(css));
            document.head.appendChild(node);

            addListener(document, "DOMContentLoaded", function () {

                {
                    var tr = query(".vBulletin_editor tr")[0];

                    var imprtBtn = document.createElement("td");
                    imprtBtn.innerHTML = "<div class='fbtn-import'></div>";
                    imprtBtn.onclick = function () {
                        var url = prompt("Введите ссылку на  фидл", "http://jsfiddle.net/");
                        if (/^https?:\/\/jsfiddle\.net\//.test(url) && ["", "user", "api"].indexOf(url.split("/")[3]) < 0)
                            opnWin(url.split("#")[0]);
                    };

                    var btn = document.createElement("td");
                    btn.innerHTML = "<div class='fbtn'></div>";
                    btn.onclick = function () {
                        opnWin()
                    };

                    tr.insertBefore(imprtBtn, query("div[id*='_cmd_resize_0_']")[0].parentNode.previousElementSibling);
                    tr.insertBefore(btn, imprtBtn);
                }
                var _FiddlePages = {};


                addListener(window, "message", listener, false);

                query(".vBulletin_editor")[0].addEventListener("click", function (event) {
                    var target = event.target;
                    if (target.classList.contains("cbfBtn")) {
                        var fidleData = _FiddlePages[target.dataset.id];
                        event.preventDefault();
                        if (target.dataset.code == "url") {
                            var text = "[URL=" + fidleData.url + "]песочница[/URL]";
                            IframeSettings.codes.forEach(function (tag) {
                                if (!fidleData[tag].length)
                                    return;
                                text += "\n" + wrapTags(tag, fidleData[tag]);
                            });
                            insertText(text);
                        } else
                            insertText(wrapTags(target.dataset.code, _FiddlePages[target.dataset.id][target.dataset.code]));
                    }
                }, true);

                function wrapTags(tag, text) {
                    return ["[", tag.toUpperCase(), "]", text, "[/", tag.toUpperCase(), "]"].join("")
                }

                function updateLink(id, obj) {
                    _FiddlePages[id] = obj;
                }

                function listener(event) {
                    console.log(event);
                    if (event.origin.indexOf("jsfiddle.net") > 0 && event.data[0] in _FiddlePages)
                        updateLink.apply(null, event.data);
                }

                function addLink(id) {
                    _FiddlePages[id] = {url: "http://jsfiddle.net/", css: "", js: "", html: ""};
                    var editor = query(".vBulletin_editor")[0];
                    var s = document.createElement("b");
                    s.innerHTML = "<div>\
                               <strong class='cbfBtn' data-code='url' data-id='" + id + "'>Fiddle #" + (Object.keys(_FiddlePages).length) + "</strong>\
                               " + (["js", "html", "css"].filter(function (el) {
                            return IframeSettings.showAllButtons || IframeSettings.codes.indexOf(el) < 0;
                        }).map(function (el) {
                            return "<a class='cbfBtn' data-code='" + el + "' data-id='" + id + "'>" + el.toUpperCase() + "</a>"
                        }).join("")) + "\
                            </div>";
                    editor.appendChild(s.firstElementChild);
                }

                function opnWin(url) {
                    var linkId = createId(url);
                    addLink(linkId);
                    window.open((url ? url : "http://jsfiddle.net/") + "#" + linkId)
                }

                function createId() {

                    return location.href.split("#")[0].replace(/[^a-f\d]/g, "") + ((new Date) - 0)
                }

                function insertText(text) {
                    var editObj = vB_Editor["vB_Editor_" + (!!document.getElementById("vB_Editor_001") ? "001" : "QR")];
                    editObj.check_focus();
                    editObj.insert_text(text, false);
                }

                // })();

                function links2frame() {
                    // превьюшки фидлвов
                    var links = [].slice.call(query("div[id^='post_message'] a[href*='jsfiddle.net']:not(.checkedLink)")),
                        postfix = "embedded/" + IframeSettings.tabs + "/" + (IframeSettings.dark ? "dark/" : "");
                    links
                        .reverse()
                        .filter(function (elem) {
                            return ["", "user", "api"].indexOf(elem.href.split("/")[3]) < 0
                        })
                        .forEach(function (target) {
                            var currentSlug, iframe, listeners, setHeight, uriEmbedded, url;

                            url = target.href.split("#")[0].replace(/\/$/, "");

                            console.log(url);

                            console.log(url.match(/\/([^\/]+?)(?:\/\d+)?$/));

                            target.addEventListener("click", function (event) {
                                event.preventDefault();
                                opnWin(url);
                            }, true);

                            uriEmbedded = url.split("/").concat(postfix).join("/");
                            currentSlug = url.match(/\/([^\/]+?)(?:\/\d+)?$/)[1]; // очень сложная и тупая регулярка

                            var ID = url.replace(/[^a-f\d]/, "");
                            var s = document.createElement("b");
                            s.innerHTML = '\
                    <div class="spoiler">\
                        <input type="checkbox" id="' + (ID) + '" ' + (IframeSettings.open ? "checked" : "") + '>\
                        <label class="alt2" for="' + ID + '">' + (target.innerText.toUpperCase()) + ':</label>\
                        <div class="text">\
                            <iframe src="' + uriEmbedded + '" width="100%" height="70" frameborder="0" \
                                sandbox="allow-forms allow-popups allow-scripts allow-same-origin allow-modals"></iframe>\
                        </div>\
                    </div>';

                            iframe = s.querySelector("iframe");
                            target.parentNode.parentNode.insertBefore(s.firstElementChild, target.parentNode.nextSibling);
                            target.classList.add("checkedLink");

                            setHeight = function (data) {
                                if (data.slug === currentSlug)
                                    return iframe.height = data.height;
                            };

                            listeners = (function () {
                                return function (event) {
                                    var data, eventName;
                                    eventName = event.data[0];
                                    data = event.data[1];
                                    if (eventName == "embed")
                                        return setHeight(data);
                                };
                            })(this);
                            return addListener(window, "message", listeners, false);
                        })
                }

                links2frame();

                var observer = new MutationObserver(function (mutations) {
                    mutations.forEach(function (mutation) {
                        if (mutation.type == "childList")
                            [].forEach.call(mutation.addedNodes, links2frame);
                    })
                });
                observer.observe(document.getElementById("posts"), {
                    childList: true
                })
            })
        })();
    else
        (function () {
            //    если на фидле, то
            if (location.hash.substr(0, 5) != "#cbef")
                return;
            var CID = location.hash.substr(1),
                ploc = location.href;

            document.addEventListener("DOMContentLoaded", function () {
                setInterval(function () {
                    console.log(getCodeFrom("panel_html"));
                    if (ploc != location.href) {
                        if (location.hash != "#" + CID)
                            location.hash = "#" + CID;
                        sendMsg();
                    }
                }, 1e3);

                sendMsg();

                function sendMsg() {
                    window.opener.postMessage([CID, {
                        url: (ploc = location.href).split("#")[0],
                        html: getCodeFrom("id_code_html"),
                        css: getCodeFrom("id_code_css"),
                        js: getCodeFrom("id_code_js")
                    }], "*");

                }

                function getCodeFrom(id) {
                    return document.getElementById(id).value
                }
            });

        })()
})(window);